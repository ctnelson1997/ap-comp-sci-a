import java.util.ArrayList;

// This class purposely introduces a lot of cool Java features
// such as for loops, enhanced for loops, and varargs.
public class Cup {
	
	private ArrayList<Dice> dice;
	
	// Create an empty cup
	public Cup() {
		this.dice = new ArrayList<>();
	}
	
	// Create a cup with a varargs (similar to an array!) of dice
	public Cup(Dice... dice) {
		
		// First create a list using the default constructor
		this();
		
		// Then use an enhanced for loop to add the dice
		for(Dice singleDice : dice) {
			this.dice.add(singleDice);
		}
	}
	
	// For each dice, roll and add to a list of rolls
	public ArrayList<Integer> roll() {
		ArrayList<Integer> rolls = new ArrayList<>();
		
		for(int i = 0; i < this.dice.size(); i++) {
			int roll = this.dice.get(i).roll();
			rolls.add(roll);
		}
		
		return rolls;
	}
	
	// Using the roll method from above, print the results!
	public void rollAndPrint() {
		ArrayList<Integer> rolls = this.roll();
		
		System.out.print("Rolling...  ");
		for(Integer roll : rolls) {
			System.out.print(roll + "  ");
		}
		
		System.out.println();
	}
}
