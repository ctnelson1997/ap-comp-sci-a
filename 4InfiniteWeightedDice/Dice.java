
public class Dice {
	
	private int numSides;
	
	// Create a new dice with a number of sides
	public Dice(int sides) {
		numSides = sides;
	}
	
	// Roll a random number between 1 and numSides!
	// Math.random returns a random number in [0,1)
	public int roll() {
		int roll = (int) (Math.random() * this.numSides) + 1;
		return roll;
	}
	
	public int getNumberOfSides() {
		return this.numSides;
	}
}
