
public class StaticMethodExamples {
	
	// You may want to comment things out to show things one at a time!
	public static void main(String[] args) {
		doMathConstants();
		doBasicMathOperations();
		doRandomThings();
		
		flipACoin();
		rollADie();
		
		printHypotenuse(3, 4);
		printHypotenuse(13, 17);
	}
	
	public static void doMathConstants() {
		System.out.println("Pi is " + Math.PI);
		System.out.println("Eulers is " + Math.E);
	}
	
	public static void doBasicMathOperations() {
		System.out.println("Sqrt of abs of -4 is " + Math.sqrt(Math.abs(-4.0f)));
		System.out.println("Max of 7.6, 4.2 is " + Math.max(7.6, 4.2));
		System.out.println("Min of 7.6, 4.2, 2.8 is " + Math.min(2.8, Math.min(7.6, 4.2)));
		System.out.println("87.1 rounded DOWN is " + Math.floor(87.1));
		System.out.println("87.1 rounded UP is " + Math.ceil(87.1));
	}
	
	public static void doRandomThings() {
		System.out.println("A random number in [0,1): " + Math.random());
		System.out.println("A random number in [0,5): " + (Math.random() * 5));
		System.out.println("A random number in [5,15): " + ((Math.random() * 10) + 5));
	}
	
	// It's a 50/50 chance! Remember Math.random returns a number in [0,1)
	public static void flipACoin() {
		if(Math.random() < 0.5) {
			System.out.println("You flipped a heads!");
		} else {
			System.out.println("You flipped a tails!");
		}
	}
	
	// How could you make this a 12-sided die, a 20-sided die?
	public static void rollADie() {
		System.out.println("You rolled a " + (int) Math.floor(((Math.random() * 6) + 1)));
	}
	
	// c = sqrt(a^2 + b^2)
	public static void printHypotenuse(float sideA, float sideB) {
		System.out.println(Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2)));
	}
}
