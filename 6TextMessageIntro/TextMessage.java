
public class TextMessage {
	private String sender;
	private String reciever;
	private String message;

	// Default Constructor
	public TextMessage() {
		sender = "John";
		reciever = "Jess";
		message = "Hello world!";
	}

	// Parameterized Constructor
	public TextMessage(String whoSent, String whoRecieved, String msg) {
		sender = whoSent;
		reciever = whoRecieved;
		message = msg;
	}
	
	// a method
	// a "getter" method
	// an accessor
	public String getTextMessage() {
		return message;
	}
	
	// a method
	// a "setter" method
	// a modifier
	// a mutator
	public void setTextMessage(String message) {
		this.message = message;
	}

	// Getter Method (general format)
	// public ______ get______() {
	// return _______;
	// }

	// Setter Method (general format)
	// public void set_______(_______ _____) {
	// _______ = _____;
	// }

	public void setSender(String send) {
		sender = send;
	}
	
	public void setReciever(String rec) {
		reciever = rec;
	}
	
	public void setMessage(String msg) {
		if(!msg.contains("bad")) {
			message = msg;
		} else {
			// Do nothing
		}
	}
	
	public String getSender() {
		return sender;
	}
	
	public String getReciever() {
		return reciever;
	}
	
	public String getMessage() {
		return message;
	}

	// TODO Display a message in the format "SENDER sent RECIEVER a message that said MESSAGE."
//	@Override
//	public String toString() {
	
//	}
}
