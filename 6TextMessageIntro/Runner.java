import java.util.Date;

public class Runner {
	public static void main(String[] args) {
		// Instantiate myMessage1 as a TextMessage with the default constructor
		TextMessage myMessage1 = new TextMessage();
		TextMessage myMessage2 = new TextMessage();
		TextMessage myMessage3 = new TextMessage();

		// Instantiate myMessage4 as a TextMessage with the parameterized constructor
		TextMessage myMessage4 = new TextMessage("Mike", "Elaine", "Hello Elaine! How are you?");

		
		myMessage3.setTextMessage("hello world!!!");
		
		String theMessage = myMessage3.getTextMessage();
		
		System.out.println(theMessage);
		
		
		System.out.println(myMessage1.getMessage());
		myMessage1.setMessage("Hello world 2!"); // message is okay!
		System.out.println(myMessage1.getMessage());
		myMessage1.setMessage("badbadbadbad"); // setMessage blocks this message
		System.out.println(myMessage1.getMessage());

		System.out.println(myMessage2.getSender());
		myMessage2.setSender("Greg"); // changes myMessage2 to be owned by Greg
		System.out.println(myMessage2.getSender());
		myMessage2.setSender("Tom"); // changes myMessage2 to be owned by Tom
		System.out.println(myMessage2.getSender());
	}
}
