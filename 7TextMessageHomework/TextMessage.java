import java.util.Date;

public class TextMessage {
	
	private String message;
	private String sender;
	private String reciever;
	private Date date;

	// Default Constructor
	public TextMessage() {
		message = "N/A";
		sender = "N/A";
		reciever = "N/A";
		date = new Date();
	}

	// Parameterized Constructor
	public TextMessage(String msg, String send, String recv) {
		message = msg;
		sender = send;
		reciever = recv;
		date = new Date();
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReciever() {
		return reciever;
	}

	public void setReciever(String reciever) {
		this.reciever = reciever;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Recv: " + date.toString() + "\n"
					+ "From: " + sender +"\n"
					+ "To: " + reciever +"\n" +
					message + "\n";
	}
}
