import java.util.Date;

public class Runner {
	public static void main(String[] args) {
		
		// Create two robots, one with name "Clark" and one with name "Hedi"
		// Although their names are "Clark" and "Hedi", we will refer
		// to them as robot1 and robot2
		
		Robot robot1 = new Robot("Clark");
		Robot robot2 = new Robot("Hedi");
		Robot robot3 = new Robot("Jess");
		
		// Send a message from robot1 to robot2
		robot1.sendMessage(robot2);
		
		// Print robot 2's latest text
		System.out.println(robot2.getLatestText());
		
		// Send a message from robot2 to robot1
		robot2.sendMessage(robot1);
		
		// Print robot 1's latest text
		System.out.println(robot1.getLatestText());
		
		robot3.sendMessage(robot1, robot2);
		robot3.sendMessage(robot2);
		
		System.out.println(robot1.getLatestText().toString() + "\n" + robot2.getLatestText().toString());
		
		//		Expected Output (Messages and dates may differ):
		//
		//		Recv: Tue Nov 10 23:11:56 CST 2020
		//		From: Clark
		//		To: Hedi
		//		Tell me more about that.
		//
		//		Recv: Tue Nov 10 23:11:56 CST 2020
		//		From: Hedi
		//		To: Clark
		//		Wow!

		// Additional problems:
		// How could we communicate with 3 robots? What about 4 robots?
		// What happens to the old text when a new text is received?
		// How could we give the robots more replies?
		// How could we make the robots "smarter"?
	}
}
