
public class Robot {
	
	private static final double NUM_RESPONSES = 5;
	
	private String name;
	private TextMessage latestText;
	
	public Robot(String n) {
		name = n;
		latestText = new TextMessage();
	}
	
	public void sendMessage(Robot otherRobot) {
		int choice = (int) (Math.random() * NUM_RESPONSES);
		TextMessage text = new TextMessage("...", this.getName(), otherRobot.getName());
		if(choice == 0) {
			text.setMessage("Very interesting!");
		} else if (choice == 1) {
			text.setMessage("Why is that?");
		} else if (choice == 2) {
			text.setMessage("Tell me more about that.");
		} else if (choice == 3) {
			text.setMessage("Could you explain?");
		} else if (choice == 4) {
			text.setMessage("Wow!");
		}
		otherRobot.recieveMessage(text);
	}
	
	// Overloaded method
	public void sendMessage(Robot otherRobot1, Robot otherRobot2) {
		this.sendMessage(otherRobot1);
		this.sendMessage(otherRobot2);
	}
	
	public void recieveMessage(TextMessage message) {
		this.latestText = message;
	}
	
	public String getName() {
		return this.name;
	}
	
	public TextMessage getLatestText() {
		return latestText;
	}
}
