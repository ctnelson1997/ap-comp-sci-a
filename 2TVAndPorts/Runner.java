
public class Runner {
	public static void main(String[] args) {
		Television tv1 = new Television(50.0f);
		
		System.out.println(tv1.getScreenSize());
		
		tv1.addPort(new VGAPort("NES"));
		tv1.addPort(new VGAPort("GameCube"));
		tv1.addPort(new VGAPort("Playstation 1"));
		tv1.addPort(new HDMIPort("Playstation 4"));
		tv1.addPort(new Port("No Device"));
		
		tv1.displayPorts();
	}
}
