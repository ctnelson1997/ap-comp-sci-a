
public class Port {
	
	private String name;
	
	public Port(String n) {
		name = n;
	}
	
	@Override
	public String toString() {
		return name + " ";
	}
}
