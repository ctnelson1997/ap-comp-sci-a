
public class VGAPort extends Port {
	
	public VGAPort(String n) {
		super(n);
	}

	@Override
	public String toString() {
		return super.toString() + "is on a VGA Port!";
	}
}
