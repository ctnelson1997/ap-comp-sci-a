
public class HDMIPort extends Port {
	
	public HDMIPort(String n) {
		super(n);
	}

	@Override
	public String toString() {
		return super.toString() + "is on an HDMI Port!";
	}
}
