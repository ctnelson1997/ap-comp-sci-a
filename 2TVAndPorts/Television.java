import java.util.ArrayList;

public class Television {
	private ArrayList<Port> ports;
	private float screenSize;
	
	public Television(float s) {
		screenSize = s;
		ports = new ArrayList<>();
	}
	
	public void addPort(Port p) {
		ports.add(p);
	}
	
	public void displayPorts() {
		for(int i = 0; i < ports.size(); i++) {
			System.out.println(ports.get(i).toString());
		}
	}
	
	public float getScreenSize() {
		return screenSize;
	}
}
