
public class BoxingUnboxingExample {
	public static void main(String[] args) {
		//  primitive vs class
		//  boolean : Boolean
		//  byte    : Byte
		//  char    : Character
		//  short   : Short
		//  int     : Integer
		//  long    : Long
		//  float   : Float
		//  double  : Double
		
		// We create an Integer object 5 that z refers to
		Integer z = new Integer(5);
		
		// We 'box' 4 into x;
		Integer x = 4;
		
		// We 'unbox' 8 into y
		int y = new Integer(8);
		
		// Because x is an object, we can call the toString method
		String myStr = x.toString();
		
		// We cannot do the same with y as it is a primitive
		// String myStr2 = y.toString();
		
		// However, we can still print them both out as Java implictly boxes y before printing
		System.out.println("x is " + x + " and y is " + y);
		
		// We cannot do this as the primitive is 'char' and associated class is 'Character'
		// There is no such thing as a 'Char'
		// Char c1 = 'a';
		
		// Time how long it takes to create and add 100000000 ints
		long primitiveStartTime = System.currentTimeMillis();
		for(int i = 0; i < 100000000; i++) {
			int throwawayVal1 = i;
			int throwawayVal2 = 4;
			int throwawayVal3 = throwawayVal1 + throwawayVal2;
		}
		long primitiveEndTime = System.currentTimeMillis();
		System.out.println("Primitive took " + (primitiveEndTime - primitiveStartTime) + " ms");
		
		// Time how long it takes to create and add 100000000 Integers
		long objectStartTime = System.currentTimeMillis();
		for(int i = 0; i < 100000000; i++) {
			Integer throwawayVal1 = new Integer(i);
			Integer throwawayVal2 = new Integer(4);
			Integer throwawayVal3 = throwawayVal1 + throwawayVal2;
		}
		long objectEndTime = System.currentTimeMillis();
		System.out.println("Object boxing took " + (objectEndTime - objectStartTime) + " ms");
	}
}
