
public class Dice {
	private int numberOfSides;
	
	public Dice() {
		numberOfSides = 6;
	}
	
	public Dice(int sides) {
		numberOfSides = sides;
	}
	
	public int roll() {
		return (int) (Math.random() * numberOfSides) + 1;
	}
	
	public int getNumberOfSides() {
		return numberOfSides;
	}
}
