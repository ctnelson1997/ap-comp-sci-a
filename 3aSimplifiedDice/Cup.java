
public class Cup {
	private Dice dice1;
	private Dice dice2;
	
	public Cup() {
		dice1 = new Dice();
		dice2 = new Dice();
	}
	
	public Cup(Dice d1, Dice d2) {
		dice1 = d1;
		dice2 = d2;
	}
	
	public Dice getDice1() {
		return dice1;
	}
	
	public Dice getDice2() {
		return dice2;
	}
	
	public int rollBothDice() {
		return dice1.roll() + dice2.roll();
	}
}
