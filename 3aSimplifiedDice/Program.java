
public class Program {
	
	public static void main(String[] args) {
		Dice d1 = new Dice(40);
		System.out.println(d1.roll());
		Dice d2 = new Dice(30);
		System.out.println(d2.roll());
		
		Cup cup = new Cup(d1, d2);
		System.out.println(cup.rollBothDice());
	}
}
