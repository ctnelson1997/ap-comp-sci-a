
// Notice WeightedDice is a type of (a child of) Dice!
public class WeightedDice extends Dice {

	// The odds that the weighted number is rolled.
	private static final float CHANCE_TO_CHEAT = 0.5f;
	
	private int weightedNumber;
	
	// Create a weighted dice with a number of sides and number to favor
	public WeightedDice(int sides, int weightedNumber) {
		super(sides);
		this.weightedNumber = weightedNumber;
	}
	
	// CHANCE_TO_CHEAT of the time, choose the weighted number.
	// Otherwise, choose a random side.
	@Override
	public int roll() {
		if(Math.random() < CHANCE_TO_CHEAT) {
			return this.weightedNumber;
		} else {
			int roll = (int) (Math.random() * this.getNumberOfSides()) + 1;
			return roll;
		}
	}

}
