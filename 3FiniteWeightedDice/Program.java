
public class Program {
	
	// Performs four different dice games
	// You may want to comment out the lines to show 1 at a time
	public static void main(String[] args) {
		doScenario1();
		doScenario2();
		doScenario3();
		doScenario4();
	}
	
	// Represents a fair game of dice!
	public static void doScenario1() {
		// Notice we use the default cup of two 6-sided dice
		Cup johnsCup = new Cup();
		Cup jillsCup = new Cup();
		
		System.out.println("John's roll!");
		johnsCup.roll();
		
		System.out.println("Jill's roll!");
		jillsCup.roll();
		
		System.out.println();
	}
	
	// Represents a fair game of dice with different types of dice!
	public static void doScenario2() {
		Cup elainesCup = new Cup(new Dice(20), new Dice(12));
		Cup ronsCup = new Cup(new Dice(20), new Dice(12));
		
		System.out.println("Elaine's roll!");
		elainesCup.roll();
		
		System.out.println("Ron's roll!");
		ronsCup.roll();
		
		System.out.println();
	}
	
	// Represents an unfair game of dice where Trudy is likely to win!
	public static void doScenario3() {
		Cup trudysCup = new Cup(new WeightedDice(6, 6), new WeightedDice(6, 6));
		Cup bobsCup = new Cup(new Dice(6), new Dice(6));
		
		System.out.println("Trudy's roll!");
		trudysCup.roll();
		
		System.out.println("Bob's roll!");
		bobsCup.roll();
		
		System.out.println();
	}
	
	// Represents an unfair game of dice where Greg is likely to lose!
	public static void doScenario4() {
		Cup sallysCup = new Cup(new Dice(12), new Dice(12));
		Cup gregsCup = new Cup(new WeightedDice(12, 1), new WeightedDice(12, 1));
		
		System.out.println("Sally's roll!");
		sallysCup.roll();
		
		System.out.println("Greg's roll!");
		gregsCup.roll();
		
		System.out.println();
	}
}
