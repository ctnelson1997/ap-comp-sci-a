
public class Cup {
	
	private Dice dice1;
	private Dice dice2;
	
	// If the user does not specify, assume normal 6-sided dice
	public Cup() {
		this.dice1 = new Dice(6);
		this.dice2 = new Dice(6);
	}
	
	// Use the dice the user wants to use!
	public Cup(Dice d1, Dice d2) {
		this.dice1 = d1;
		this.dice2 = d2;
	}
	
	// For each dice, roll and print the results
	public void roll() {
		int roll1 = this.dice1.roll();
		int roll2 = this.dice2.roll();
		System.out.println("Rolled a " + roll1 + " and a " + roll2 + "!");
	}
}
